#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
sys.path.append(os.curdir)
from pelicanconf import *

SITEURL = 'https://micronews.debian.org'

DELETE_OUTPUT_DIRECTORY = True

# Following items are often useful when publishing

# Uncomment following line for absolute URLs in production:
RELATIVE_URLS = True

#DISQUS_SITENAME = ""
#GOOGLE_ANALYTICS = ""
