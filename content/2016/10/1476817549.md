Title: LWN: Supporting UEFI secure boot in Debian
Slug: 1476817549
Date: 2016-10-20 10:00
Author: Ana Guerrero López
Status: published

LWN: Supporting UEFI secure boot in Debian [https://lwn.net/Articles/703001/](https://lwn.net/Articles/703001/)
