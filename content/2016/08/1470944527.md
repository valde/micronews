Title: Tor Project took inspiration from Debian for its social contract https://lists.debian.org/debian-project/2016/08/msg00012.html
Slug: 1470944527
Date: 2016-08-11 19:42
Author: Ana Guerrero López
Status: published

Tor Project took inspiration from Debian for its social contract [https://lists.debian.org/debian-project/2016/08/msg00012.html](https://lists.debian.org/debian-project/2016/08/msg00012.html)
