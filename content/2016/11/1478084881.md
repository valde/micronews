Title: New mailing list created for Debian events in the Asia/Pacific region https://lists.debian.org/debian-events-apac/
Slug: 1478084881
Date: 2016-11-02 11:08
Author: Paul Wise
Status: published

New mailing list created for Debian events in the Asia/Pacific region [https://lists.debian.org/debian-events-apac/](https://lists.debian.org/debian-events-apac/)
