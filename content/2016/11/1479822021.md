Title: If you used to be a DD and want to access nm.debian.org (e.g. to reactivate your account), you can now do it using your Alioth credentials https://lists.debian.org/debian-newmaint/2016/11/msg00026.html
Slug: 1479822021
Date: 2016-11-22 13:40
Author: Laura Arjona Reina
Status: published

If you used to be a DD and want to access nm.debian.org (e.g. to reactivate your account), you can now do it using your Alioth credentials [https://lists.debian.org/debian-newmaint/2016/11/msg00026.html](https://lists.debian.org/debian-newmaint/2016/11/msg00026.html)
