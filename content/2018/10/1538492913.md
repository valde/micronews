Title: Bits from the Release Team: Freeze timeline, fix RC bugs and changes to testing migration rules https://lists.debian.org/debian-devel-announce/2018/09/msg00004.html
Slug: 1538492913
Date: 2018-10-02 15:08
Author: Laura Arjona Reina
Status: published

Bits from the Release Team: Freeze timeline, fix RC bugs and changes to testing migration rules [https://lists.debian.org/debian-devel-announce/2018/09/msg00004.html](https://lists.debian.org/debian-devel-announce/2018/09/msg00004.html)
