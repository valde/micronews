Title: Updated Debian 8: 8.11 released https://www.debian.org/News/2018/20180623
Slug: 1529785547
Date: 2018-06-23 20:25
Author: Donald Norwood
Status: published

Updated Debian 8: 8.11 released [https://www.debian.org/News/2018/20180623](https://www.debian.org/News/2018/20180623)
