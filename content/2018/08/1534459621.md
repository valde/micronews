Title: Several Debian designers created artwork to celebrate #DebianDay #Debian25years - This is by Angelo Rosa https://bits.debian.org/images/debian25years.png
Slug: 1534459621
Date: 2018-08-16 22:47
Author: Laura Arjona Reina
Status: published

Several Debian designers created artwork to celebrate #DebianDay #Debian25years - This is by Angelo Rosa [https://bits.debian.org/images/debian25years.png](https://bits.debian.org/images/debian25years.png) 

![Debian is 25 years old by Angelo Rosa](|filename|/images/Debian25years-AngeloRosa.png)
