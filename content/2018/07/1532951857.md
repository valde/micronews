Title: Last "working" sessions today at #DebConf18: Server freedom, News from the APT team, Debian & XMPP, and OpenRC https://debconf18.debconf.org/schedule/?day=2018-07-30
Slug: 1532951857
Date: 2018-07-30 11:57
Author: Laura Arjona Reina
Status: published

Last "working" sessions today at #DebConf18: Server freedom, News from the APT team, Debian & XMPP, and OpenRC [https://debconf18.debconf.org/schedule/?day=2018-07-30](https://debconf18.debconf.org/schedule/?day=2018-07-30)
