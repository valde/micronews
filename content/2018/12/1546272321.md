Title: Bits from the @Debian Project Leader (December 2018) -- https://lists.debian.org/debian-devel-announce/2018/12/msg00006.html
Slug: 1546272321
Date: 2018-12-31 16:05
Author: Chris Lamb
Status: published

Bits from the @Debian Project Leader (December 2018) -- [https://lists.debian.org/debian-devel-announce/2018/12/msg00006.html](https://lists.debian.org/debian-devel-announce/2018/12/msg00006.html)
