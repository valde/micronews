Title: New Debian Developers and Maintainers (July and August 2017) https://bits.debian.org/2017/09/new-developers-2017-08.html
Slug: 1504371976
Date: 2017-09-02 17:06
Author: Laura Arjona Reina
Status: published

New Debian Developers and Maintainers (July and August 2017) [https://bits.debian.org/2017/09/new-developers-2017-08.html](https://bits.debian.org/2017/09/new-developers-2017-08.html)
