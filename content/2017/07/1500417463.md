Title: The schedule for DebConf's Open Day on August 5 in Montreal, Canada is available https://debconf17.debconf.org/schedule/open-day/
Slug: 1500417463
Date: 2017-07-18 22:37
Author: Cédric Boutillier
Status: published

The schedule for DebConf's Open Day on August 5 in Montreal (Canada) is available [https://debconf17.debconf.org/schedule/open-day/](https://debconf17.debconf.org/schedule/open-day/)
