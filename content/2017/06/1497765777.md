Title: Pixar is making new Toy Story episodes faster than Debian releases consume them. There are at least 62 toy names left! https://lists.debian.org/debian-curiosa/2014/11/msg00005.html #releasingstretch
Slug: 1497765777
Date: 2017-06-18 06:02
Author: Paul Wise
Status: published

Pixar is making new Toy Story episodes faster than Debian releases consume them. There are at least 62 toy names left! [https://lists.debian.org/debian-curiosa/2014/11/msg00005.html](https://lists.debian.org/debian-curiosa/2014/11/msg00005.html) #releasingstretch
