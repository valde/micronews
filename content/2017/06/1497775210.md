Title: Read the full stretch release announcement at https://www.debian.org/News/2017/20170617 #releasingstretch
Slug: 1497775210
Date: 2017-06-18 08:40
Author: Paul Wise
Status: published

Read the full stretch release announcement at [https://www.debian.org/News/2017/20170617](https://www.debian.org/News/2017/20170617) #releasingstretch
