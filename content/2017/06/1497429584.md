Title: Debian Installer Stretch RC 5 release https://lists.debian.org/debian-devel-announce/2017/06/msg00003.html
Slug: 1497429584
Date: 2017-06-14 08:39
Author: Laura Arjona Reina
Status: published

Debian Installer Stretch RC 5 release [https://lists.debian.org/debian-devel-announce/2017/06/msg00003.html](https://lists.debian.org/debian-devel-announce/2017/06/msg00003.html)
