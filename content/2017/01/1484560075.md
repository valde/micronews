Title: The root CA for Let's Encrypt (ISRG root X1) is now part of Debian's Jessie, Stretch and Sid releases https://letsencrypt.org/2016/08/05/le-root-to-be-trusted-by-mozilla.html
Slug: 1484560075
Date: 2017-01-16 09:47
Author: Laura Arjona Reina
Status: published

The root CA for Let's Encrypt (ISRG root X1) is now part of Debian's Jessie, Stretch and Sid releases [https://letsencrypt.org/2016/08/05/le-root-to-be-trusted-by-mozilla.html](https://letsencrypt.org/2016/08/05/le-root-to-be-trusted-by-mozilla.html)
