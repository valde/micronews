Title: More DebConf17 talks: Rough times? TUF shines, Debian and Outreachy, Use Perl (Annual meeting of the Debian Perl Group) https://debconf17.debconf.org/schedule/?day=2017-08-10
Slug: 1502388636
Date: 2017-08-10 18:10
Author: Laura Arjona Reina
Status: published

More DebConf17 talks: Rough times? TUF shines, Debian and Outreachy, Use Perl (Annual meeting of the Debian Perl Group) [https://debconf17.debconf.org/schedule/?day=2017-08-10](https://debconf17.debconf.org/schedule/?day=2017-08-10)
